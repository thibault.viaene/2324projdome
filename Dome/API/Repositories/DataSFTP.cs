﻿using Renci.SshNet;
using API.Entities;

namespace API.Repositories
{
    public class DataSFTP
    {
        private readonly string host = "192.168.1.253";
        private readonly string username = "sftpuser";
        private readonly string password = "sftpUs3r";
        private static readonly HashSet<string> AllowedExtensions = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            ".txt", ".jpg", ".png", ".pdf", ".sh"
        };

        public List<SftpFile> ListFiles(string remoteDirectory)
        {
            var files = new List<SftpFile>();
            using (var client = new SftpClient(host, username, password))
            {
                client.Connect();
                foreach (var entry in client.ListDirectory(remoteDirectory))
                {
                    if (!entry.Name.StartsWith("."))
                    {
                        var isPreviewable = IsPreviewable(entry.Name);
                        files.Add(new SftpFile
                        {
                            Name = entry.Name,
                            FullPath = entry.FullName,
                            Size = entry.Attributes.Size,
                            IsDirectory = entry.IsDirectory,
                            IsPreviewable = isPreviewable
                        });
                    }
                }
                client.Disconnect();
            }
            return files;
        }

        private bool IsPreviewable(string fileName)
        {
            return AllowedExtensions.Contains(Path.GetExtension(fileName).ToLower());
        }
    }
}