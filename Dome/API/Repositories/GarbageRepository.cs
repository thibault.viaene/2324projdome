﻿using Globals;
using API.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Globals.Models.User;
using Globals.Models.TaskSection;
using Globals.Models.TaskItem;
using System;
using API.Helpers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace API.Repositories
{
	public interface IGarbageRepository
	{
		Task<List<WasteCollectionTask>> GetGarbage();
		Task<List<WasteCollectionTask>> PostGarbage(List<WasteCollectionTask> tasks);
	}
	public class GarbageRepository : IGarbageRepository
	{
		private readonly DataBaseContext _context;
		public GarbageRepository(DataBaseContext context)
		{
			_context = context;
		}
		public async Task<List<WasteCollectionTask>> GetGarbage()
		{
			return await _context.WasteCollectionTasks
			   .Select(x => new WasteCollectionTask
			   {
				   Datum = x.Datum,
				   Label = x.Label
			   })
			   .AsNoTracking()
			   .ToListAsync();
		}

		public async Task<List<WasteCollectionTask>> PostGarbage(List<WasteCollectionTask> tasks)
		{
			await _context.WasteCollectionTasks.AddRangeAsync(tasks);
			await _context.SaveChangesAsync();
			return tasks;
		}
	}
}
