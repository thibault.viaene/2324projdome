﻿using API.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Globals.Models.TaskSection;
using Globals.Models.User;

namespace API.Controllers
{
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ApiController]
    public class TaskSectionsController : ControllerBase
    {
        private readonly ITaskSectionRepository _taskSectionRepository;

        public  TaskSectionsController(ITaskSectionRepository taskSectionRepository)
        {
            _taskSectionRepository = taskSectionRepository;
        }

        [HttpGet(Name = "GetTaskSections")]
        //[Authorize(Roles = "admin")]
        public async Task<List<GetTaskSectionModel>> Get()
        {
            return await _taskSectionRepository.GetTaskSections();
        }

        [HttpGet("{id}")]
		
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		//[Authorize(Roles = "admin,user")]
		public async Task<List<GetTaskSectionModel>> Get(Guid id)
        {
            return await _taskSectionRepository.GetTaskSection(id);
        }

        [HttpPut(Name = "PutTaskSection")]
        //[Authorize(Roles = "admin")]
        public async Task Put(Guid id, PutTaskSectionModel putTaskSectionModel)
        {
            await _taskSectionRepository.PutTaskSection(id, putTaskSectionModel);
        }

        [HttpPost(Name = "PostTaskSection")]
        //[Authorize(Roles = "admin")]
        public async Task<GetTaskSectionModel> Post(PostTaskSectionModel postTaskSectionModel)
        {
            return await _taskSectionRepository.PostTaskSection(postTaskSectionModel);
        }

		[HttpDelete("{id}")]
		//[Authorize(Roles = "admin")]
		public async Task<IActionResult> Delete(string id)
		{
			string response = await _taskSectionRepository.DeleteTaskSection(Guid.Parse(id));

			if (response.Equals("y"))
			{
				return Ok("Task deleted succesfully");
			}
			return StatusCode(500, $"Internal server error: {response}");
		}
	}
}
