﻿using API.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Globals.Models.User;
using Globals.Models.User;

namespace API.Controllers
{
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpDelete("{id}")]
        //[Authorize(Roles = "admin")]
        public async Task<IdentityResult> Delete(string id)
        {
            return await _userRepository.DeleteUser(id);
        }

        //[Authorize(Roles = "admin")]
        [HttpGet(Name = "GetUsers")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<List<GetUserModel>> Get()
        {
            return await _userRepository.GetUsers();
        }

        [HttpGet("{id}")]
        //[Authorize(Roles = "admin,user")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<GetUserModel> Get(string id)
        {
            return await _userRepository.GetUser(Guid.Parse(id));
        }

        [HttpPut(Name = "PutUser")]
        //[Authorize(Roles = "admin")]
        public async Task<IActionResult> Put(string id, PutUserModel putUserModel)
        {
            bool result = await _userRepository.PutUser(id, putUserModel);
            if (result)
            {
                
                return NoContent();
            }
            return NotFound();
        }

        [HttpPost(Name = "PostUser")]
        [Consumes("application/json")]
        //[Authorize(Roles = "admin")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<GetUserModel>> Post(PostUserModel postUserModel)
        {
            GetUserModel getUserModel = await _userRepository.PostUser(postUserModel);
            
            return getUserModel;
        }

        [HttpPatch(Name = "PatchUser")]
        //[Authorize(Roles = "admin")]
        public async void Patch(string id, PatchUserModel patchUserModel)
        {
            await 
                _userRepository.PatchUser(id, patchUserModel);
        }

        [AllowAnonymous]
        [HttpPost("Authenticate")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [Consumes("application/json")]
        public async Task<ActionResult<PostAuthenticationResponseModel>> Authenticate(PostAuthenticationRequestModel postAuthenticationRequestModel)
        {
            try
            {
                PostAuthenticationResponseModel postAuthenticationResponseModel = await _userRepository.Authenticate(postAuthenticationRequestModel, IpAddress());
                SetTokenCookie(postAuthenticationResponseModel.RefreshToken);
                return postAuthenticationResponseModel;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost("renew-token")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<PostAuthenticationResponseModel>> RenewToken()
        {
            try
            {
                string refreshToken = Request.Cookies["Restaurant.RefreshToken"];
                PostAuthenticationResponseModel postAuthenticationResponseModel = await _userRepository.RenewToken(refreshToken, IpAddress());
                SetTokenCookie(postAuthenticationResponseModel.RefreshToken);
                return postAuthenticationResponseModel;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("deactivate-token")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        //[Authorize(Roles = "admin")]
        public async Task<IActionResult> DeactivateToken(PostDeactivationTokenRequestModel postDeactivationTokenRequestModel)
        {
            try
            {
                string token = postDeactivationTokenRequestModel.Token ?? Request.Cookies["Restaurant.RefreshToken"];
                if (string.IsNullOrEmpty(token))
                {
                    throw new Exception("Refresh token is mandatory");
                }

                await _userRepository.DeactivateToken(token, IpAddress());
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        private void SetTokenCookie(string token)
        {
            CookieOptions cookieOptions = new()
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddMinutes(5),
                IsEssential = true
            };

            Response.Cookies.Append("Restaurant.RefreshToken", token, cookieOptions);

        }

        private string IpAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
            {
                return Request.Headers["X-Forwared-For"];
            }
            else
            {
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
            }
        }


    }
}
