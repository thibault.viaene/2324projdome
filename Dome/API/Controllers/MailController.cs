﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using API.Entities;
using API.Repositories;
using Microsoft.AspNetCore.Mvc;
using Globals;

namespace API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class MailController : ControllerBase
	{
		private readonly IMailRepository _mailRepository;

		public MailController(IMailRepository mailRepository)
		{
			_mailRepository = mailRepository;
		}

		[HttpGet("/api/hasmail")]
		public async Task<MailItem> Get()
		{
			return await _mailRepository.GetMail();
		}
	}
}
