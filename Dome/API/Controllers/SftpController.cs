﻿using API.Entities;
using API.Repositories;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Renci.SshNet;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace API.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class FtpController : ControllerBase
	{
		private static string host = "192.168.1.253";
		private static string username = "sftpuser";
		private static string password = "sftpUs3r";
		private readonly DataSFTP _ftpService = new DataSFTP();

		[HttpGet("list-files")]
		public async Task<IActionResult> ListFiles(string path)
		{
			try
			{
				var files = _ftpService.ListFiles(path);
				return Ok(files);
			}
			catch (Exception ex)
			{
				return StatusCode(500, $"Internal server error: {ex.Message}");
			}
		}

		[HttpGet("download-file")]
		public async Task<IActionResult> DownloadFile(string path)
		{
			var client = new SftpClient(host, username, password);
			try
			{
				client.Connect();
				var fileStream = client.OpenRead(path);
				var fileName = Path.GetFileName(path);

				// Determine the content type based on the file extension
				var contentType = GetContentType(fileName);

				// Prepare the file stream and return the file
				// Ensure the stream will be disposed by converting to MemoryStream
				var memoryStream = new MemoryStream();
				await fileStream.CopyToAsync(memoryStream);
				memoryStream.Position = 0;  // Reset the position after copying

				return File(memoryStream, contentType, fileName);
			}
			finally
			{
				client.Disconnect();
				client.Dispose();
			}
		}

		private string GetContentType(string fileName)
		{
			var provider = new Microsoft.AspNetCore.StaticFiles.FileExtensionContentTypeProvider();
			if (!provider.TryGetContentType(fileName, out var contentType))
			{
				contentType = "application/octet-stream";  // Default to binary if unknown
			}
			return contentType;
		}

		[HttpPost("upload-file")]
		public async Task<IActionResult> UploadFile(IFormFile file, string path)
		{
			if (file == null || file.Length == 0)
				return BadRequest("No file uploaded.");

			using (var client = new SftpClient(host, username, password))
			{
				try
				{
					client.Connect();
					if (client.IsConnected)
					{
						if (!path.EndsWith("/"))
						{
							path += "/";
						}

						string fullPath = path + file.FileName;
						if (!client.Exists(fullPath))
						{
							using (var fileStream = file.OpenReadStream())
							using (var sftpStream = client.Create(fullPath))
							{
								await fileStream.CopyToAsync(sftpStream);
							}
						}
						else
						{
							return StatusCode(500, $"File already exists");
						}
						return Ok("File uploaded successfully.");
					}
					else
					{
						return StatusCode(500, "Failed to connect to SFTP server.");
					}
				}
				catch (Exception ex)
				{
					return StatusCode(500, $"Internal server error: {ex.Message}");
				}
				finally
				{
					client.Disconnect();
					client.Dispose();
				}
			}
		}

		[HttpDelete("delete-file")]
		public async Task<IActionResult> DeleteItem(string path)
		{
			try
			{
				using (var client = new SftpClient(host, username, password))
				{
					client.Connect();
					if (client.Exists(path))
					{
						if (client.GetAttributes(path).IsDirectory)
						{
							client.DeleteDirectory(path);
						}
						else
						{
							client.DeleteFile(path);
						}
					}

					client.Disconnect();
				}

				var directory = Path.GetDirectoryName(path);
				return Ok("File deleted successfully.");
			}
			catch (Exception ex)
			{
				return StatusCode(500, $"Internal server error: {ex.Message}");
			}
		}
	}
}
