﻿namespace API.Entities
{
    public class SftpFile
    {
        public string Name { get; set; }
        public string FullPath { get; set; }
        public long Size { get; set; }
        public bool IsDirectory { get; set; }
        public bool IsPreviewable { get; set; }
    }

}
