﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Emit;
using Globals;

namespace API.Entities
{
    public class DataBaseContext : IdentityDbContext<
        User,
        Role,
        Guid,
        IdentityUserClaim<Guid>,
        UserRole,
        IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>,
        IdentityUserToken<Guid>
        >


    {
        public DbSet<TaskItem> TaskItems { get; set; }
        public DbSet<TaskSection> TaskSections { get; set; }
        //public DbSet<User> Users { get; set; }
        //public DbSet<Role> Roles { get; set; }
        //public DbSet<UserRole> UsersRoles { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
		public DbSet<DoorheenDeDagVerbruik> doorheendedagverbruik { get; set; }
		public DbSet<MailItem> mail { get; set; }
		public DbSet<Address> address { get; set; }
		public DbSet<WasteCollectionTask> WasteCollectionTasks { get; set; }

		public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

           
            builder.Entity<TaskSection>(x =>
            {
                x.HasMany(x => x.TaskItems)
                .WithOne(x => x.TaskSection)
                .HasForeignKey(x => x.TaskSectionId)
                .OnDelete(DeleteBehavior.Cascade);

            });

			builder.Entity<WasteCollectionTask>()
			.HasKey(w => new { w.Datum, w.Label});

			builder.Entity<WasteCollectionTask>()
			.HasIndex(w => new { w.Datum, w.Label })
			.IsUnique();

			builder.Entity<User>(x =>
            {
                x.HasMany(x => x.TaskSections)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);

                x.HasMany(x => x.UserRoles)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<Role>(x =>
            {
                x.HasMany(x => x.UserRoles)
                .WithOne(x => x.Role)
                .HasForeignKey(x => x.RoleId)
                .OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<RefreshToken>(x =>
            {
                x.HasOne(x => x.User)
                .WithMany(x => x.RefreshTokens)
                .HasForeignKey(x => x.UserId)
                .IsRequired();
            });
        }
    }
}
