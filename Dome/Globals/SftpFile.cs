﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals
{
    public class SftpFile
    {
        public string Name { get; init; }
        public string Identifier { get; set; }
        public string FullPath { get; set; }
        public long Size { get; set; }
        public bool IsDirectory { get; set; }
        public bool IsPreviewable { get; set; }
    }
}
