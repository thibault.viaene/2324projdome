﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals
{
	public class DoorheenDeDagVerbruik
	{
		public int Id { get; set; }
		public float DagVerbruik { get; set; }
		public float NachtVerbruik { get; set; }
		public float TotaalVerbruik { get; set; }
		public DateTime Timestamp { get; set; }
	}
}
