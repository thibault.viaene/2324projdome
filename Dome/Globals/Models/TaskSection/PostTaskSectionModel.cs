﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals.Models.TaskSection
{
    public class PostTaskSectionModel : BaseTaskSectionModel
    {
        public Guid UserId { get; set; }
    }
}
