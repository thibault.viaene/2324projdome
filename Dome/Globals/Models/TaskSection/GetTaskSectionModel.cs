﻿using Globals.Models.TaskItem;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals.Models.TaskSection
{
    public class GetTaskSectionModel : BaseTaskSectionModel
    {
        [Required]
        [RegularExpression(@"(?im)^[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?$")]
        public Guid Id { get; set; }
        public IEnumerable<GetTaskItemModel> GetTaskItemModels { get; set; }
        public Guid UserId { get; set; }
    }
}
