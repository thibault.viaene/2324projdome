﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals.Models.TaskItem
{
    public class GetTaskItemModel : BaseTaskItemModel
    {
        [Required]
        public Guid Id { get; set; }
    }
}
