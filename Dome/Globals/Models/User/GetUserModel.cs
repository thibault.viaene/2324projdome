﻿using Globals.Models.TaskSection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Globals.Models.User
{
    public class GetUserModel : BaseUserModel
    {
        [Required]
        [RegularExpression(@"(?im)^[{(]?[0-9A-F]{8}[-]?(?:[0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?$")]
        public Guid Id { get; set; }

        public string Email { get; set; }

        public ICollection<GetTaskSectionModel>? GetTaskSectionModels { get; set; }

		public List<string> Roles { get; set; }
	}
}
