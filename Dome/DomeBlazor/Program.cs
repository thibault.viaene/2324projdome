using DomeBlazor;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor;
using MudBlazor.Services;
using DomeBlazor.Services;
using DataImplementations;
using Microsoft.AspNetCore.Components.Authorization;



var builder = WebAssemblyHostBuilder.CreateDefault(args);

builder.Services.AddMudServices();
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri("https://localhost:7123") });
builder.Services.AddScoped<CustomAuthenticationStateProvider>();
builder.Services.AddScoped<AuthenticationStateProvider>(provider => provider.GetRequiredService<CustomAuthenticationStateProvider>());

builder.Services.AddAuthorizationCore(config =>
{
	config.AddPolicy("AdminOnly", policy => policy.RequireRole("admin"));
	config.AddPolicy("UserOnly", policy => policy.RequireRole("user"));
	config.AddPolicy("MustBeLoggedIn", policy => policy.RequireAuthenticatedUser());
});

var host = builder.Build();
await host.RunAsync();